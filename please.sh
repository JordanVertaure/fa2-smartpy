#! /bin/sh

set -e
usage () {
    cat >&2 <<EOF

EOF
}

get_smartpy_recommended_version () {
    echo dev-20200721-726c0e95f0a586cf4e62fc371c11d8436b081290
}
local_smartpy_install_path=$PWD/_smartpy_installation
get_smartpy_local_install_path () {
    echo "$local_smartpy_install_path"
}
get_smartpy () {
    smartpy_version="$(please.sh get_smartpy_recommended_version)"
    curl -s https://SmartPy.io/$smartpy_version/cli/SmartPy.sh -o /tmp/SmartPy.sh
    sh /tmp/SmartPy.sh local-install-auto "$local_smartpy_install_path"
}
export PATH=$PATH:"$local_smartpy_install_path"

check () {
    commands="opam SmartPy.sh tezos-client flextesa"
    for c in $commands ; do
        command -v $c >/dev/null || {
            echo "Error missing command: $c"
            echo "the project needs in \$PATH: $commands"
            exit 2
        }
    done
}

prepare () {
    check
    if ! [ -d _opam/ ] ; then
        opam switch create . ocaml-base-compiler.4.09.1
    fi
    eval $(opam env)
    opam pin add -n angstrom 0.13.0
    opam install -y base fmt uri cmdliner ezjsonm \
         ocamlformat uri merlin ppx_deriving angstrom
}
eval $(opam env)

build_default () {
    # We want some error messages earlier:
    SmartPy.sh compile multi_asset.py \
               'FA2(environment_config(),sp.address("tz1asCc9HRuVkSdkw5eYF7vWiefpd5hCCWdG"))' \
               $PWD/_test_output
    SmartPy.sh test multi_asset.py  $PWD/_test_output
    sh src/app/gen_dune.sh > src/app/dune.inc
    #dune build @autogen --auto-promote
    dune build src/app/main.exe
    dune build @src/app/contracts
    ln -fs _build/default/src/app/main.exe fatoo
    echo "## Build Successful"
    find _build/default/src/app/ -name '*.tz'
}
build_tutorial () {
    output=${1:-_build/tutorial}
    pid_file=$output/sandbox.pid
    mkdir -p "$output"
    export alice="$(flextesa key alice)"
    export bob="$(flextesa key bob)"
    if [ "$no_sandbox" != "true" ] ; then
        flextesa mini-net \
                 --size 2 --time-b 2 --number-of-boot 2 --until 20_000_000 \
                 --add-bootstrap-account="$alice@2_000_000_000_000" \
                 --add-bootstrap-account="$bob@2_000_000_000_000" \
                 > "$output/sandbox.log" 2>&1 &
        echo $! > $pid_file
        echo "SANDBOX: $pid_file -> $(cat $pid_file), sleeping for a bit..."
        sleep 4
    fi
    tezos-client config reset
    tezos-client -P 20000 config update
    tezos-client bootstrapped
    for i in $(seq 1 10) ; do
        tezos-client rpc get /chains/main/blocks/1/protocols && break || {
                echo "Sandbox not ready"
                ps $(cat "$pid_file")
                sleep 3
            }
    done
    fatoo show-tuto -o "$output/fa2-tutorial.md" $tutorial_extra_options
    if [ "$no_sandbox" != "true" ] ; then
        echo "Killing sandbox"
        kill "$(cat $pid_file)"
    fi
    echo "See: $output/fa2-tutorial.md"
}

build_codedoc () {
    run make-implementation-document multi_asset.py _build/multi_asset.md
}

make_css () {
    cat > "$1" <<EOF
body {font-family: sans;}
/* pre {
  display:block;
  padding: 4px;
  overflow: auto;
  background-color: #eee;
  border: solid #aaa 1px;
  margin-left: 2px;
} */
pre {
  background-color: #eee;
  border: solid #aaa 1px;
}
blockquote {border-left: solid 2px #aaa; padding-left: 1em}
EOF
}

pandocize () {
    echo "Making $2"
    pandoc -s --toc -i "$1" --to html5 -o "$2" --css ./style.css
}

build_website () {
    output=${1:-_build/website}
    tutorial=${2:-_build/tutorial/fa2-tutorial.md}
    mkdir -p "$output"
    make_css "$output/style.css"
    pandocize "$tutorial" "$output/tutorial.html"
    pandocize "README.md" "$output/implementation.html"
    build_codedoc
    pandocize "_build/multi_asset.md" "$output/literate_code_document.html"
    cat > /tmp/index.md <<EOF
---
title: FA2-SmartPy — Home
---

See:

* [Tutorial](./tutorial.html)
* [Implementation entry-point](./implementation.html)
* [Literate Code documentation](./literate_code_document.html)
EOF
    pandocize /tmp/index.md "$output/index.html"
    echo "Done: file://$PWD/$output/index.html"
}

build () {
    case "$1" in
        "" ) build_default ;;
        * )
            c="$1"
            shift
            "build_$c" "$@" ;;
    esac
}

install () {
    bindir=${PREFIX:-/usr}/bin
    mkdir -p $bindir
    cp -f _build/default/src/app/main.exe $bindir/fatoo
    chmod a+rx $bindir/fatoo
}


run () {
    case "$1" in
        "simulations" )
            dune build @src/app/simulations
            echo "-------------------"
            echo "Done:"
            for txt in $(find _build/default/src/app/ -name '*simulation-log.txt')
            do
                echo "$txt: $(grep -i error $txt | wc -l) errors"
            done
            ;;
        * )
            dune exec src/app/main.exe -- "$@" ;;
    esac
}

update_michelsons () {
    datetag="$(date -u +%Y%m%d-%H%M%S%z)"
    gittag="$(git describe --always --dirty)"
    dirpath="michelson/"
    path_prefix="$dirpath/${datetag}_${gittag}"
    readme="michelson/README.md"
    mkdir -p "$dirpath"
    echo "Generated Michelson Contracts" > "$readme"
    echo "=============================" >> "$readme"
    printf "\n\nLatest builds as pure Michelson files:\n\n" >> "$readme"
    all="contract dbg_contract mutran_contract perdesc_noops_contract single_mutran_contract nft_mutran_contract"
    #all="contract dbg_contract"
    for c in $all; do
        path="${path_prefix}_$c.tz"
        echo "Making '$path'"
        run get-code "$c" -o "$path"
        desc="$(./please.sh run list --details desc --format tsv | grep -E "^$c" | cut -f 2)"
        printf "* \`%s\`: %s.\n" "$(basename $path)" "$desc" >> "$readme"
    done
    printf "\n\nSee also \`fatoo list-contract-variants --help\` for all available builds and details about them:\n\n" >> "$readme"
    run list-contract-variants --details all --format markdown >> "$readme"
}
update () {
    update_"$1" "$@"
}

lint () {
    find src \( -name "*.ml" -o -name "*.mli" \) -exec ocamlformat -i {} \;
}


{
    if [ "$1" = "" ]; then
        build
    else
        "$@"
    fi
}

